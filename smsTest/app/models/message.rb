class Message < ApplicationRecord
	validates :sms_number, 
					presence: true,
                    :length => { 	minimum: 10, 
                    			maximum:10 },
                    :format => {:with => /^[0-9]{10}$/i, 
                    			:multiline => true,
                    			:message => "Phone number must be 10 digits, no dashes or non-numerical characters."}
end
