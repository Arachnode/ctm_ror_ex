class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.string :sms_number
      t.text :sms_text

      t.timestamps
    end
  end
end
