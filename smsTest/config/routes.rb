Rails.application.routes.draw do
  get 'send_sms/index'

  resources :messages

  get 'welcome/index'

  root 'welcome#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
